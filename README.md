git_clone
=========

Clone git repositories.

Requirements
------------

Repositories are public accessible or authentication is provided.
Git executable must be installed.

Role Variables
--------------

| Variable                      | Required | Description                                                                             | Default |
|-------------------------------|----------|-----------------------------------------------------------------------------------------|---------|
| git_clone__projects_directory | true     | Directory on your disk where the repositories will be placed.                           | ''      |
| git_clone__flat_projects      | true     | List of repositories which will be placed in directly in git_clone__projects_directory. | []      |

Dependencies
------------

```yaml
  - src: https://gitlab.com/ibox-project/roles/ibox.configuration.git
    version: stable
    scm: git
    name: ibox.configuration
```

Example Playbook
----------------
Include the role in `requirements.yml` with something like
```yaml
  - src: https://gitlab.com/ibox-project/roles/ibox.git_clone.git
    version: stable
    scm: git
    name: ibox.git_clone
```

and write in the playbook something like

```yaml
  tasks:
    - name: "Include ibox.git_clone"
      ansible.builtin.include_role:
        name: "ibox.git_clone"
      vars:
        git_clone__projects_directory: '~/project'
        git_clone__flat_projects:
          - "git@gitlab.com:ibox-project/roles/ibox.git_clone.git"

```

License
-------

Licensed under the [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0)
